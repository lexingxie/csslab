---
layout: name
title: People

section: Home
---

<a id="top"></a>

CSS@CS: Who we are
=========================

The team at ANU-CSS lab consist of several faculty, postdocs, PhD students and visitors. We have diverse sets expertise, including machine learning, complex systems, networks, continuous and discrete optimisation, decision making under uncertainty, simulation and visualisation, and computational social choice.

> 
<a href=#cs>CS faculty</a> | <a href=#socialsci>Social Science collaborators</a> | <a href=#students>Students</a> | <a href=#joinus>Join Us!</a>
<hr />

<a id="cs"></a>
<img class='inset left' src='./images/PascalVanHentenryck.jpg' title='Pascal Van Hentenryck' alt='
Pascal' height='100px' border='0px' />

**[Pascal Van Hentenryck][pascal]**: Chair Professor in [Data-Intensive Computing][pascal_chair] at the Australian National University (ANU). Leader of the optimization research group at NICTA (about 60 people). <br />
_Interests:_ Continuous and discrete optimisation, decision making under uncertainty, simulation and visualisation, and computational social choice.<br />
_Projects:_ Disaster management, future energy systems, and supply-chains. <br />
_Before the ANU:_ professor of computer science at Brown University for about 20 years. <br />

<img class='inset left' src='./images/lexing-color.jpg' title='Lexing Xie' alt='Lexing' height='100px' border='0px' />

[**Lexing Xie**][lexing]: Senior lecturer at the ANU; machine learning group at NICTA. <br />
*Interests:* Applied machine learning, multimedia, social media. <br />
*Projects:* Predicting popularity on social media; 
[visual memes on YouTube](http://users.cecs.anu.edu.au/~xlx/proj/visualmemes.html);
[a network of picture tags](http://users.cecs.anu.edu.au/~xlx/proj/tagnet); 
[mapping the macrosopic structure of the event web](http://www.aaai.org/ocs/index.php/ICWSM/ICWSM12/paper/view/4595).
<br />
*Before the ANU:* IBM T J Watson Research Center <br />


<img class='inset left' src='./images/manuel_nicta.jpg' title='Manuel Cebrian' alt='Manuel' height='100px' border='0px' />

**[Manuel Cebrian][manuel]**: Senior scientist with NICTA, and adjunct faculty at the ANU. <br />
*Interests:* Social and financial networks, crowdsourcing, urban economics, behavioral game theory, and evolutionary dynamics -- he specializes in solving problems at the intersection of the computer and social sciences. <br />
*Projects:* [Social sensing for epidemiological behavior change](http://ginger.io), [evolution of social ties in cities](http://www.nature.com/ncomms/2013/130604/ncomms2961/full/ncomms2961.html), [the red balloon challenge](http://web.mit.edu/newsoffice/2011/red-balloons-study-102811.html).  <br />
*Before the ANU:* postdoctoral positions both in academia -- UC San Diego, MIT and Brown University, and industry -- Telefonica's and Facebook's data science teams. <br />


<p>&nbsp;<p/><a id="socialsci"></a>

<img class='inset left' src='https://researchers.anu.edu.au/researchers/ackland-rj/image' title='Robert Ackland' alt='
Ackland' height='100px' border='0px' />

**[Robert Ackland][roba]**: Associate Professor and Deputy Director (Education), Australian Demographic and Social Research Institute, ANU. <br />
_Interests:_ Network science, web science, social network analysis, Index number theory, International comparisons of income and poverty. Leads the [VOSON Project](http://voson.anu.edu.au/).<br />
_Before the ANU:_ Bureau of Immigration Research, consulting for World Bank, AusAID, Asian Development Bank. 


<img class='inset left' src='https://crawford.anu.edu.au/sites/default/files/styles/anu_wide_320_320/public/pictures/robert_costanza.jpg?itok=cdhzjdxv' title='Robert Costanza' alt='
Costanza' height='100px' border='0px' />

**[Robert Costanza][costanza]**: Chair Professor in Public Policy, ANU. <br />
_Interests:_ Integrating the study of humans and the rest of nature to address research, policy and management issues at multiple time and space scales, from small watersheds to the global system.<br />
_Specialties:_ Transdisciplinary integration, systems ecology, ecological economics, landscape ecology, ecological modeling, ecological design, energy analysis, environmental policy, social traps, incentive structures and institutions. <br />
_Before the ANU:_ Portland State U, Vermont University, Maryland, LSU. 

<img class='inset left' src='http://darrenhalpin.com/wp-content/uploads/2013/10/darrenhalpin.jpeg' title='Darren Halpin' alt='
Halpin' height='100px' border='0px' />

**[Darren Halpin](http://darrenhalpin.com/)**: Associate Professor Research School of Social Sciences, ANU. <br />
_Interests:_  Studying interest groups in the policy process, with specific emphasis on the political representation provided by groups, the level of (and necessity for) internal democracy within groups, and in assessing group organizational development/capacity.<br />
_Before the ANU:_ University of Århus, The Robert Gordon University (Scotland). 


<p>&nbsp;<p/><a id="students"></a>

<img class='inset left' src='./images/alex.png' title='Alex' alt='PhD student' height='100px' border='0px' />
**Alexander Mathews**: PhD Student, ANU <br />
_Interests:_ applied machine learning, multimedia, computer vision. <br />
_Projects:_ multimedia analysis, visual semantics. <br />
_Before the ANU:_ University of Western Australia <br />

<img class='inset left' src='./images/shahin.jpg' title='Shahin' alt='PhD student' height='100px' border='0px' />
**Shahin Namin**: PhD Student, ANU <br />
_Interests:_ applied machine learning, social media <br />
_Projects:_ spatial-temporal recommendataion <br />
_Before the ANU:_ University of Tehran, Iran <br />

<img class='inset left' src='./images/honglin.png' title='Honglin' alt='PhD student' height='100px' border='0px' />
**Honglin Yu**: PhD Student, ANU <br />
_Interests:_ applied machine learning, social media <br />
_Projects:_ popularity prediction in social media <br />
_Before the ANU:_ Southeast University, China <br />

<img class='inset left' src='./images/qianyu.jpg' title='Qianyu' alt='student' height='100px' border='0px' />
**Qianyu Zhang**: Honor Student, ANU <br />
_Interests:_ applied machine learning, natural language processing, computer vision. <br />
_Projects:_ predicting words from pictures<br />



<p>&nbsp;<p/><a name='joinus'></a>
<img class='inset left' src='./images/people_name.png' title='Join Us!' alt='Recruiting' height='100px' border='0px' />
**We are recruiting!** 

> We are looking for new team members who are:
> passionate about computational social science, 
> and have a solid computational and mathematical background. 
See [**this page**](joinus.html) for more information, and how to apply. 

<hr />
><a href=./>home</a> | <a href=#top>back to top</a> 
<p>&nbsp;<p/>
<p>&nbsp;<p/>

[costanza]:https://crawford.anu.edu.au/people/academic/robert-costanza
[roba]:https://researchers.anu.edu.au/researchers/ackland-rj
[pascal]:https://dl.dropboxusercontent.com/u/62188928/site/Welcome.html
[pascal_chair]:http://news.anu.edu.au/2013/08/14/new-chair-in-data-intensive-computing/
[lexing]:http://cecs.anu.edu.au/~xlx/
[manuel]:http://web.media.mit.edu/~cebrian/
