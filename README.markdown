
CSS@CS Lab Website Readme
==============

Jan 2014 - 


This repo hosts the source files of the CSS@CS lab of ANU (http://css.cecs.anu.edu.au). 

The rest of this readme is for the web-master, about:
* how to setup the workflow
* what to do if you need to make changes. 

This repo uses the style of <http://mark.reid.name> 
it uses Jekyll content management system (to generate a set of static webpages), plus pandoc for markdown rendering, and MathJax for latex equations. 


A quick overview for user/editor
-----------

Once setup, this repo should be fairly straight forward to add/change, as follows.

To add/edit a post: add/edit a markdown file under "./_posts/"

To change the webpage boilerplate, go to "./_includes" and "./_layouts"
	javascripts and css are under "./files"

To compile, preview, and serve the pages, read on for _three simple_ steps. 



Installing Jekyll, markdown, and other relevant environment tools
-----------

### for OS X

0) First you need a working setup of ruby and gems. 

I'm on OS X Mountain Lion that comes with ruby:
$ruby -v
ruby 1.8.7 (2012-02-08 patchlevel 358) [universal-darwin12.0]

$ gem -v
2.0.3

1) sudo gem install jekyll

2) installing pandoc as the markdown renderer
   (pandoc is great. the author John MacFarlane is a ninja, therefore haskell .... )

2.1) install GHC platform (200M+ download )
	http://www.haskell.org/platform/

2.2) install pandoc -- markdown rendering

downloaded pandoc dmg from here and installed, now it works.
http://johnmacfarlane.net/pandoc/installing.html
https://code.google.com/p/pandoc/downloads/list

cabal update
cabal install pandoc
-- didn't seem to work ("which pandoc" gave empty returns)


2.3) get pandoc to work inside ruby + jekyll 
 first install the pandoc ruby plugin "sudo gem install pandoc-ruby" 
 then install jekyll pandoc plugin https://github.com/dsanson/jekyll-pandoc-plugin (done for this repo)

3) add the following to javascript.html to enable mathjax (already done for this repo)
	```html
    <!-- Equations using MathJax -->
    <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script type="text/x-mathjax-config"> MathJax.Hub.Config({ TeX: { equationNumbers: {autoNumber: "all"} } });       </script>
    ```

     and {% include javascript.html %} to default.html 


### for Ubuntu

0) install ruby
```bash
sudo apt-get install ruby
```

1) install jekyll
```bash
sudo gem install jekyll
```

2) install pandoc

if you are using Ubuntu 13.04, you can not install haskell-platform from `apt` and need to upgrade the system or compile it from source.
Following tips are tested on Ubuntu 12.04

2.1) install haskell platform
```bash
sudo apt-get install haskell-platform
```

2.2)
```bash
cabal update
cabal install pandoc
```

2.3) get pandoc to work inside ruby + jekyll 
 first install the pandoc ruby plugin "sudo gem install pandoc-ruby" 
 then install jekyll pandoc plugin https://github.com/dsanson/jekyll-pandoc-plugin (done for this repo)

3) add the following to javascript.html to enable mathjax (already done for this repo)
	```html
    <!-- Equations using MathJax -->
    <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script type="text/x-mathjax-config"> MathJax.Hub.Config({ TeX: { equationNumbers: {autoNumber: "all"} } });       </script>
    ```

     and {% include javascript.html %} to default.html 


lessons
---
the default markdown renderer doesn't work so well with mathjax
sudo gem install maruku

changing to kramdown with the advise here:
http://dasonk.github.io/blog/2012/10/09/Using-Jekyll-and-Mathjax/
didn't seem to work either


Change the local html environment 
-----------

To change the configuration for your custom deployment, make a _config.yml 
   you can make a copy of _config.example.yml and then change field values as necessary.

e.g. 
change line 3 of _config.yml to point to your local dir for the compiled htmls
destination : /Users/xlx/Public/css2013


Compile and Serve this Site
========
type this from the current dir:
jekyll serve

see that there's no complaints.

and then:
either preview it at http://localhost:4000
OR http://localhost/target_dir if you've added target_dir to apache httpd.conf
OR rsync target_dir to a remote server


License
=======

This is the licence from Mark.Reid.name

Unless otherwise attributed, all of the content in this repository is released under a Creative Commons ([Attribution-Noncommercial-Share Alike 3.0 Unported](http://creativecommons.org/licenses/by-nc-sa/3.0/) licence.

Usage (from mark reid)
=====
I develop the site locally and then rsync the changes to my host as follows:

    rsync -ave ssh ~/Sites/mark.reid.name/ confla@conflate.net:www/name


Thanks
======
A huge thanks to Tom Preston-Werner for creating Jekyll.

Mark Ried for the style;

and Carl Boettiger for the working example + code for Jekyll + Pandoc
http://carlboettiger.info/lab-notebook.html
