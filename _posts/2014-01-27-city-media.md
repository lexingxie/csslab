---
title: CSS@CS - Project Topic

location: Canberra, Australia

layout: tut-post
draft: true
---


What does a city talk about?
===================

Goal
------------
This project develops a system to track the evolution of different political topics overtime, as discussed in one or more modern city. Such a system will eventually be used to answer questions about topic longevity, the long-term pattern of public policy propaganda, and understanding the citizens' response to these issues. 


Method
------------
Start by tracking news article links in geo-referenced tweet streams. 



Background and skills
------------
The student participant are expected to be very self-motivated, possess solid background in math and programming. 
The student will work in a team, and acquire or already have the following sets of skills. 

* be comfortable programming with web service APIs;
* be familiar with the state-of-the-art tools of hyper-text and natural language analysis;
* be familiar with basic statsitical analysis and interpreting their results;
* design experiments, perform meausrements and draw conclusions in computational social sciences. 


References
------------

* [Kim, Xie, Christern 2012] "Event Patterns in Social Media", ICWSM 2012
* [Leskovec et al 2009] MemeTracker, KDD 2009
* [Leskovec et al 2013] NIFTY, WWW 2013
