---
layout: tut
title: CSS2013 slides
section: Home

keywords: Machine Learning, Computational Social Science, Course, Tutorial
---

Day 1 
----------

* [Introduction and Motivations for CSS](handout/day1-intro.pdf)
* [Network Representation](handout/day1-networkrep.pdf)
* [Network Visualization with iGraph](handout/igraph.pdf)
* [ANU Team introduction](handout/ANU-team.pdf)


Day 2
----------

* Introduction to NLP, IR, and machine learning ([pdf](handout/day2.pdf))
* Reuters news dataset for the practical session ([zip](handout/reuters.zip)). 
It is highly recommended that you **pre-download this data before the afternoon session** to avoid network congestion in the classroom.
* [Example code for the practical session](handout/prac.py).  You may have different code, of course, and different ideas.
* [Output from this code](handout/prac.output).


Day 3
----------

* [Slides and further information for statistical network analysis](http://voson.anu.edu.au/css2013-day3) 
* Information Transfer in Social Media, Ver Steeg and Galstyan, WWW 2012 ([abstract](http://dl.acm.org/citation.cfm?id=2187906)) ([pdf](http://www2012.wwwconference.org/proceedings/proceedings/p509.pdf) - 11.8MB, we recommend that you download this before the afternoon session)

Project Checkpoint on Day 3

* If you **haven't chosen a project topic**, do so **ASAP**.
* If you **haven't spoken to your project mentor**, do so **today**.
* If you **don't know what method your project will be using**, figure out **by end of today** by talking to lectures and TAs.
  This will often require reading 1 or 2 papers, **get started ASAP** if you haven't. 
* If you **don't know what data you will be using**, figure out **by Thursday morning**.


Day 4
----------

* [Slides for lecture on Dynamic Networks](handout/dynamic_networks.pdf) (Note that this contains links to film clips which are not contained in the PDF, but which will be shown in the class.)
* For the practical session:
	* We will be investigating a model from Stattner et al (2011), ["Diffusion in Dynamic Social Networks: Application in Epidemiology"](handout/stattner_lncs_2011.pdf), Lecture Notes in Computer Science v 6861, pp. 559-573;
	* From the lecture, [here](handout/sir.py) is a simulation of an SIR model using networkx in Python;
	* From the lecture, [here](handout/visualise_sir.r) visualises the networks created from the Python script above.

	* Participant Yi Zong created a very nice solution for the practical session and with his agreement I am placing his contribution here, as an example of how to meet the stated goals. You may find his additions to the sir.py code [here](handout/sir_yizong.py).  


Day 5
----------

* 9am - 1:30pm Work on your projects and prepare presentations (lunch provided)
   	* Presentation:
   		* Every team is allocated 5 minutes plus 2 minutes for questions and presentation switching. 
   	 	* Your 5 minutes can be allocated as: problem statement and how you did it (2.5 min), your results so far, and what was the most interesting/revealing/surprising/difficult (1.5 min), outlook: what more can you do if ... you had another week, more data, etc (1 min)
   	 	* It is up to you how many people in your team will present, and who presents what.

* 1:30pm - 2pm break
* 2:00pm - 3:30pm Project presentations
	* The order of the presentations is posted [here](proj.html)
	* You also need to review other presentations with [the form on the project page](proj.html#vote)

* 3:30pm - 4pm 

	* Students: fill out an _anonymous_ online course feedback [here][css_feedback]
   	* Lectures: tally the project votes, decide on prizes

* 4pm - 4:30pm
	* Prizes, group photo, wrap up     


[css_feedback]:https://docs.google.com/forms/d/1UO_2FatIArYJ5aIqJxvzurnMcNSEvvA06RUB1nV4dHM/viewform
