---
layout: name
title: Projects

section: Home
---

CSS@CS: Projects and Publications
=========================

For up-to-date publication lists, please see the following. 
<ul>
	<li>Pascal Van Hentenryck - 
		[[publications]](https://dl.dropboxusercontent.com/u/62188928/site/Publications.html)
		[[google scholar]](http://scholar.google.com/citations?hl=en&user=FTSZV7kAAAAJ)
		[[dblp page]](http://www.informatik.uni-trier.de/~ley/db/indices/a-tree/h/Hentenryck%3APascal_Van.html)
	</li>
	<li>Manuel Cebrian - 
		[[homepage]](http://web.media.mit.edu/~cebrian/)
		[[google scholar]](http://scholar.google.com.au/citations?user=ZkBTPkUAAAAJ&hl=en)
		[[dblp page]](http://www.informatik.uni-trier.de/~ley/pers/hd/c/Cebri=aacute=n:Manuel.html)
	</li>
	<li>Lexing Xie - 
		[[publications]](http://cecs.anu.edu.au/~xlx/publications)
		[[google scholar]](http://scholar.google.com.au/citations?user=u0xUDSoAAAAJ&hl=en)
		[[dblp page]](http://www.informatik.uni-trier.de/~%20ley/pers/hd/x/Xie:Lexing.html)
	</li>
</ul>


Here are a few example probjects carried out by the group over the past few years.

<img class='inset left' src="./images/DiscreetOptimisation_logo.png" 
  width=120>
**Teaching Creative Problem Solving in a MOOC** <br/>
Proceedings of the SIGCSE, Atlanta, USA, pp. 6, March, 2014.  <br/>
C. Coffrin, P. Van Hentenryck
[[abstract+paper](http://www.nicta.com.au/pub?id=7482)] 
<br/>

<img class='inset left' src="./images/nyc.jpg" 
  width=120>
**Urban characteristics attributable to density-driven tie formation**  <br/>
Nature Communications 4, 1961 (2013) 
[[paper link](http://www.nature.com/ncomms/2013/130604/ncomms2961/full/ncomms2961.html)] <br/>
W. Pan, G. Ghoshal, C. Krumme, M. Cebrian, and A. Pentland <br/>
selected press: 
[nytimes](http://6thfloor.blogs.nytimes.com/2013/06/10/innovation-bout-beijing-vs-zurich) |
[economist](http://www.economist.com/blogs/babbage/2013/06/urbanisation) |
[smithsonian](http://blogs.smithsonianmag.com/ideas/2013/06/why-living-in-a-city-makes-you-more-innovative/) | 
[mit news](http://web.mit.edu/newsoffice/2013/why-innovation-thrives-in-cities-0604.html)
<br/>

<img class='inset left' src="./images/meme-influence.png" 
  width=120>
**Visual Meme in Social Media: Tracking News in YouTube Videos**<br/>
ACM Multimedia 2011, IEEE Trans. Multimedia 2013 <br/>
L. Xie, A. Natsev, X. He, J. Kender, M. Hill, J. Smith <br/>
[project page](http://users.cecs.anu.edu.au/~xlx/proj/visualmemes.html) | 
[paper](http://arxiv.org/abs/1210.0623) |
[slides](http://users.cecs.anu.edu.au/~xlx/proj/memepic/visual-memes-MM11.pdf) <br/>


<img class='inset left' src="./images/red_balloon.jpg" 
  width=120>
**Time-critical social mobilization**  <br/>
Science 334(6055):509-512 (2011) 
[[paper link](http://web.media.mit.edu/~cebrian/Science-2011-Pickard-509-12.pdf)] <br/>
 G. Pickard, W. Pan, I. Rahwan, M. Cebrian, R. Crane, A. Madan, and A. Pentland<br/>
selected press: 
[nytimes](http://www.nytimes.com/2009/12/07/technology/internet/07contest.html) |
[msnbc](http://www.msnbc.msn.com/id/45078320/ns/technology_and_science-science/#.TqueXUA0p4V) |
[popular mechanics](http://www.popularmechanics.com/technology/military/research/what-darpas-scavenger-hunt-taught-us-about-mobilizing-people-fast) | 
[mit news](http://web.mit.edu/newsoffice/2011/red-balloons-study-102811.html)
<br/>

<img class='inset left' src="./images/evacuation.png" 
  width=120>
**A Conflict-Based Path-Generation Heuristic for Evacuation Planning** <br/>
Technical Report VRL-7393, NICTA, Melbourne, Australia, 2013. <br/>
V. Pillac, P. Van Hentenryck, C. Even <br />
[arxiv](http://arxiv.org/abs/1309.2693) |
[pdf](http://arxiv.org/pdf/1309.2693v1) <br/>


<img class='inset left' src="http://users.cecs.anu.edu.au/~xlx/proj/tagnet/1000words.png" 
  width=120>
**Picture Tags and World Knowledge**  <br/>
ACM Multimedia 2013 <br/>
L. Xie, X. He <br/>
[project page](http://cecs.anu.edu.au/~xlx/proj/tagnet) | 
[paper](href=http://cecs.anu.edu.au/~xlx/papers/mm2013-xie.pdf) | 
[slides](http://cecs.anu.edu.au/~xlx/proj/tagnet/mm2013-tagnet.pdf) | 
[anu reporter coverage](http://news.anu.edu.au/2012/11/26/lost-and-found/)<br/>

<img class='inset left' src="http://users.cecs.anu.edu.au/~xlx/proj/event-sphere.png" 
  width=120>
**Mapping the Macroscopic Structure of the Event Web**  <br/>
ICWSM 2012 <br/>
M. Kim, L. Xie, P. Christen <br/>
[abstract](http://www.aaai.org/ocs/index.php/ICWSM/ICWSM12/paper/view/4595) |
[paper](http://users.cecs.anu.edu.au/~xlx/papers/icwsm12-event.pdf) <br/>


<img class='inset left' src="./images/solar.png" 
  width=120>
**Residential Demand Response under Uncertainty** <br />
Intl. Conf. on Principles and Practice of Constraint Programming (CP), Sweden, Sept. 2013. <br />
P. Scott, S. Thiebaux, M. van den Briel, P. Van Hentenryck <br />
[slides](http://cp2013.a4cp.org/slides/80.pdf) |
[paper](http://users.cecs.anu.edu.au/~thiebaux/papers/greencoplas13.pdf) <br/>

<img class='inset left' src="./images/nyt_phone.png" 
  width=120>
**Social sensing for epidemiological behavior change**  <br/>
ACM Ubicomp 2010 <br/>
A. Madan, M. Cebrian, D. Lazer, and A. Pentland<br/>
[paper link](http://web.media.mit.edu/%7Ecebrian/sensing.pdf) | 
[ginger.io](http://ginger.io/) |
[nature news coverage](http://www.nature.com/news/computational-social-science-making-the-links-1.11243)
<br/>



