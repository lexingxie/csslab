---
layout: name
title: News

section: Home
---

CSS@CS: News
=========================

Here is a list of all news, events, and updates related to the CSS@CS lab. 

<ul>
  {% for post in site.posts %}
  {% unless post.draft %}
  <li><a href="{{ site.url }}{{ post.url }}">{{ post.title }}</a> ({{ post.date | date: '%d %B, %Y'}})</li>
  {% endunless %}
  {% endfor %}
</ul>

