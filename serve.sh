# script for building and deploying the site
# The ~ here refers to ~xlx (softlinked to the actual web dir)
# change as you see fit

jekyll build

ssh cpu.cecs.anu.edu.au -f 'rm -R ~/csslab/*'

rsync -avz /Users/xlx/Public/CSS-lab-site/* cpu.cecs.anu.edu.au:~/csslab/

# ssh cpu.cecs.anu.edu.au -f 'rm -R ~/web/csslab/*'; jekyll build; rsync -avz /Users/xlx/Public/CSS-lab-site/* cpu.cecs.anu.edu.au:~/web/csslab/

#ssh cpu.cecs.anu.edu.au -f 'rm -R ~/csslab/*'; jekyll build; rsync -avz /Users/xlx/Public/CSS-lab-site/* cpu.cecs.anu.edu.au:~/csslab/
