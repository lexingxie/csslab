---
layout: name
title: Home

section: Home
---


<!--
  <img class='inset right' src='./images/anu-logo.jpg' title='ANU logo' alt='ANU logo' width='150px' />
-->

Welcome! The Computational Social Science (CSS@CS) Lab is located within the [Research School of Computer Science][cecs] at the [Australian National University][anu]. We collaborate closely with the [Machine Learning][nicta_ml] and [Optimization][nicta_opt] research groups in [NICTA][nicta], and a growing set of ANU researchers in the social sciences. 

We focus on laying the computational foundations from large amounts of social and behavioral data. The outcome of our work puts a strong emphasis on actionable insights, and in the long-term influence on policy. 


What we do
------------------

Here are a few examples of our recent projects. [This page](proj.html) contains a larger and more diverse list.

<img class='inset left' src="./images/nyc.jpg" 
  width=120>
**Urban characteristics attributable to density-driven tie formation**  <br/>
Nature Communications 4, 1961 (2013) 
[[paper link](http://www.nature.com/ncomms/2013/130604/ncomms2961/full/ncomms2961.html)] <br/>
W. Pan, G. Ghoshal, C. Krumme, M. Cebrian, and A. Pentland <br/>
selected press: 
[nytimes](http://6thfloor.blogs.nytimes.com/2013/06/10/innovation-bout-beijing-vs-zurich) |
[economist](http://www.economist.com/blogs/babbage/2013/06/urbanisation) |
[smithsonian](http://blogs.smithsonianmag.com/ideas/2013/06/why-living-in-a-city-makes-you-more-innovative/) | 
[mit news](http://web.mit.edu/newsoffice/2013/why-innovation-thrives-in-cities-0604.html)
<br/>

<img class='inset left' src="./images/evacuation.png" 
  width=120>
**A Conflict-Based Path-Generation Heuristic for Evacuation Planning** <br/>
Technical Report VRL-7393, NICTA, Melbourne, Australia, 2013. <br/>
V. Pillac, P. Van Hentenryck, C. Even <br />
[arxiv](http://arxiv.org/abs/1309.2693) |
[pdf](http://arxiv.org/pdf/1309.2693v1) <br/>


<img class='inset left' src="./images/meme-influence.png" 
  width=120>
**Visual Meme in Social Media: Tracking News in YouTube Videos**<br/>
ACM Multimedia 2011, IEEE Trans. Multimedia 2013 <br/>
L. Xie, A. Natsev, X. He, J. Kender, M. Hill, J. Smith <br/>
[project page](http://users.cecs.anu.edu.au/~xlx/proj/visualmemes.html) | 
[paper](http://arxiv.org/abs/1210.0623) |
[slides](http://users.cecs.anu.edu.au/~xlx/proj/memepic/visual-memes-MM11.pdf) <br/>


<img class='inset left' src="./images/red_balloon.jpg" 
  width=120>
**Time-critical social mobilization**  <br/>
Science 334(6055):509-512 (2011) 
[[paper link](http://web.media.mit.edu/~cebrian/Science-2011-Pickard-509-12.pdf)] <br/>
 G. Pickard, W. Pan, I. Rahwan, M. Cebrian, R. Crane, A. Madan, and A. Pentland<br/>
selected press: 
[nytimes](http://www.nytimes.com/2009/12/07/technology/internet/07contest.html) |
[msnbc](http://www.msnbc.msn.com/id/45078320/ns/technology_and_science-science/#.TqueXUA0p4V) |
[popular mechanics](http://www.popularmechanics.com/technology/military/research/what-darpas-scavenger-hunt-taught-us-about-mobilizing-people-fast) | 
[mit news](http://web.mit.edu/newsoffice/2011/red-balloons-study-102811.html)
<br/>

Who we are
------------------

The CSS@CS team consists of several faculty, postdocs, PhD students and visitors. We have diverse sets of expertise, including machine learning, complex systems, networks, continuous and discrete optimisation, decision making under uncertainty, simulation and visualisation, and computational social choice. Meet us on [this page](people.html).

<a href='people.html'>
<img class='inset left' src='./images/PascalVanHentenryck_bw.jpg' title='Pascal Van Hentenryck' alt='Pascal' height='80px' border='0px' />
<img class='inset left' src='./images/lexing-coast.jpg' title='Lexing Xie' alt='Lexing' height='80px' border='0px' />
<img class='inset left' src='./images/manuel_nicta_bw.jpg' title='Manuel Cebrian' alt='Manuel' height='80px' border='0px' />
<img class='inset left' src='./images/boy-face-bw.png' title='PhD student 1' alt='PhD student' height='80px' border='0px' />
<img class='inset left' src='./images/girl-bw-md.png' title='PhD student girl' alt='PhD student' height='80px' border='0px' />
</a>
<a href='people.html#joinus'>
<img class='inset left' src='./images/people_name.png' title='Join Us!' alt='Recruiting' height='80px' border='0px' />
</a>


News and updates
------------------

<ul>
  {% for post in site.posts limit:25 %}
  {% unless post.draft %}
  <li><a href="{{ site.url }}{{ post.url }}">{{ post.title }}</a> ({{ post.date | date: '%d %B, %Y'}})</li>
  {% endunless %}
  {% endfor %}
</ul>

Events and meetings
-------------------
<ul>
	Weekly research group meeting, Mondays 1:30pm CSIT N234
</ul>

[nicta]:http://www.nicta.com.au/
[nicta_ml]:http://www.nicta.com.au/research/machine_learning
[nicta_opt]:http://optimisation.nicta.com.au/
[anu]:http://www.anu.edu.au/
[cecs]:http://cecs.anu.edu.au/
[rscs]:http://cs.anu.edu.au/


<!--
test image hover, doesn't work 


<div id="pascal" height='100px', width='100px'></div>

<style>
#pascal {
   background-image: url('./images/PascalVanHentenryck_bw.jpg');
   height: 100px;
}
#pascal:hover {
   background-image: url('./images/PascalVanHentenryck.jpg');
}
</style>


<svg xmlns="http://www.w3.org/2000/svg" id="svgroot" viewBox="0 0 400 377" width="400" height="377">
  <defs>
     <filter id="filtersPicture">
       <feComposite result="inputTo_38" in="SourceGraphic" in2="SourceGraphic" operator="arithmetic" k1="0" k2="1" k3="0" k4="0" />
       <feColorMatrix id="filter_38" type="saturate" values="0" data-filterid="38" />
    </filter>
  </defs>
  <image filter="url(&quot;#filtersPicture&quot;)" x="0" y="0" width="400" height="377" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="http://4.bp.blogspot.com/-IzPWLqY4gJ0/T01CPzNb1KI/AAAAAAAACgA/_8uyj68QhFE/s1600/a2cf7051-5952-4b39-aca3-4481976cb242.jpg" />
   </svg>
-->
